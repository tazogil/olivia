$(function(){
  // bouton navigation portable
  $(document).on("click", ".bouton-menu", function (e) {
    $("nav").slideToggle();
  });
  // CLICK POUR AFFICHER LE TEXTE SUR L'ACTION (HOVER NE MARCHE PAS)
  // remplacement des a[href] par l'ID de l'actu
  $(".visuel-actu").each(function(index){
    $(this).children("a").attr("href", "#" + $(this).attr("id"));
  });
  // click sur le visuel : aff le chapeau
  $(".visuel-actu a").click(function(e){
    e.stopPropagation();
    $(this).parent().hide();
    $(".chapeau-actu").show();
  });
  // click sur le chapeau : aff le visuel
  $(".chapeau-actu").click(function(e){
    e.stopPropagation();
    $(this).hide();
    $(".visuel-actu").show();
  });

})
